

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PetShop PetStyle</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
</head>
<body>
    
   <section class="container topo">
          <div class="logo">
              <img src="images/LOGOTIPO PET.png" alt="logo tipo da empresa petStyle">
          </div>
        <div class="topo-inner">  
            <div class="frase-topo">
                <h2>A vida é mais <span>alegre</span> com um pet!<i class="fa fa-paw" aria-hidden="true"></i></h2>
            </div>
         </div>
   </section>
    
 <section class="container main">
      <div class="main-left">
          <div class="main-left-title">
              <h1>Seu <span>pet</span> e nosso <span>shop</span>, a <span>combinação</span> perfeita para o cuidado com seus animais.</h1> 
          </div>
          <div class="main-left-text">
              <h2>Todo cuidado para seu melhor amigo sempre é pouco, que tal deixar ele <span>cheiroso</span> e comportado.</h2>
          </div>
          <div class="main-left-promocao">
               <img src="images/promocao.png" alt="promoção da petshop">
          </div>
      </div>
      <div class="main-right">
           
           <form method="post" class="formulario">
                <img src="images/form-CAO.png" alt="">
                <h4>Cadastre-se agora e receba: 1 cupom promocional e 1 vídeo <span class="detalhe">AUAU</span>la de treinamento!</h4>
                <input type="email" name="email" placeholder="Insira seu E-mail">
                <input type="text" name="nomepet" placeholder="Informe o nome do seu pet"/>
                <input type="text" name="nome" placeholder="Informe seu nome"/>
                <div class="icon">
                    <i class="fa fa-whatsapp"  aria-hidden="true"></i> <input type="text" name="whatsapp" placeholder="informe seu whatsapp">
                </div>
                <input type="submit" value="Enviar">
                
           </form>

      </div>
 </section>


</body>
</html>